#Stage 1
FROM node:latest as build
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run build --prod
#Stage 2
FROM nginx:alpine
COPY --from=build /app/dist/frontend/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8080
